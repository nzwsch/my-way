require 'ext/float'

describe :to_score do
  it do
    -1.to_score.should  == '00000000'
  end
  
  it do
    0.to_score.should   == '00000000'
  end

  it do
    0.5.to_score.should == '00000000'
  end

  it do
    1.to_score.should   == '00000100'
  end
  
  it do
    100.to_score.should == '00010000'
  end
end
