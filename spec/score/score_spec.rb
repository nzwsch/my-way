require 'score'

module Score
  describe self do
    context :DATA do
      it { Score::DATA.should == Dir.pwd + '/data' }
    end
  end

  describe Lank do
    before :each do
      @lank = Lank.new
    end

    context :list do
      it { @lank.list.should be_a Array }
    end

    context :high_score do
      it { @lank.high_score.should be_a Fixnum }
    end
  end
end
