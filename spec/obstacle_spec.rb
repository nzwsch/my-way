require 'spec_helper'
require 'game/obstacle'

module Game
  describe Obstacle do
    before :each do
      @window = MockWindow.new
    end

    context :pos_y do
      it do
        o = Game::Obstacle.new(@window, 0, 0, ::Image.new(@window), :white, 10)
        o.pos_y.should == 0
      end

      it do
        pending
        o = Game::Obstacle.new(@window, 0, 0, ::Image.new(@window), 0)
        o.property.should  == {
          type:  :obstacle,
          color: :white,
          val:   OBSTACLE_DAMAGE_VALUE
        }
      end

      it do
        pending
        o = Game::Obstacle.new(@window, 0, 0, ::Image.new(@window), 1)
        o.property.should  == {
          type:  :obstacle,
          color: :black,
          val:   OBSTACLE_DAMAGE_VALUE
        }
      end

      it do
        pending
        o = Game::Obstacle.new(@window, 0, 0, ::Image.new(@window), 2)
        o.property.should  == {
          type:  :item,
          color: :white,
          val:   10
        }
      end

      it do
        pending
        o = Game::Obstacle.new(@window, 0, 0, ::Image.new(@window), 3)
        o.property.should  == {
          type:  :item,
          color: :black,
          val:   10
        }
      end
    end

    context :conditions do
      it 'white obstacle' do
        pending
        o = Game::Obstacle.new(@window, 0, 0, ::Image.new(@window), 0)
        p = ::Player.new; p.crash = true; o.update p
        @window.score.should == 0
      end

      it 'black obstacle' do
        pending
        o = Game::Obstacle.new(@window, 0, 0, ::Image.new(@window), 1)
        p = ::Player.new; p.crash = true; o.update p
        p.window.score.should == OBSTACLE_DAMAGE_VALUE
      end

      it 'white item' do
        pending
        o = Game::Obstacle.new(@window, 0, 0, ::Image.new(@window), 2)
        p = ::Player.new; p.crash = true; o.update p
        p.window.score.should == 10
      end

      it 'black item' do
        pending
        o = Game::Obstacle.new(@window, 0, 0, ::Image.new(@window), 3)
        p = ::Player.new; p.crash = true; o.update p
        p.window.score.should == 0
      end
    end

    context :asdf do
    end

    context :gone? do
      it do
        o = Game::Obstacle.new(@window, 0, 0, ::Image.new(@window), :white, 10)
        o.gone?.should be_false
      end

      it do
        o = Game::Obstacle.new(@window, 0, 0, ::Image.new(@window), :white, 10)
        p = ::Player.new; 1025.times { o.update p }
        o.gone?.should be_true
      end
      
      it "doesn't matter when player was crashed" do
        o = Game::Obstacle.new(@window, 0, 0, ::Image.new(@window), :white, 10)
        p = ::Player.new; p.crash = true; o.update p
        o.gone?.should be_false
      end
    end
  end
end
