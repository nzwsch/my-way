require 'spec_helper'
require 'game/counter'

module Game
  describe Counter do
    before :each do
      @counter = Counter.new
    end

    it do
      @counter.interval.should == 0
    end

    it do
      @counter.second.should == 0
    end

    context 'update once' do
      it do
        @counter.update
        @counter.interval.should == 1
      end
    end

    context 'update 60 times' do
      before :each do
        60.times { @counter.update }
      end

      it do
        @counter.interval.should == 0
      end

      it do
        @counter.second.should == 1
      end
    end
  end
end
