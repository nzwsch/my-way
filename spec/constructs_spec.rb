require 'yaml'
require 'game/constructs'

describe Game do
  context 'ROOT' do
    it { Game::ROOT.should == Dir.pwd }
  end

  context 'LEVELS_DIR' do
    it { Game::LEVELS_DIR.should == File.join(Dir.pwd, 'levels') }
  end

  context 'LEVELS' do
    it 'should == 3 levels of dat' do
      Game::LEVELS.should == [
        File.join(Dir.pwd, 'levels', 'level1.dat'),
        File.join(Dir.pwd, 'levels', 'level2.dat'),
        File.join(Dir.pwd, 'levels', 'level3.dat')
      ]
    end
  end
end
