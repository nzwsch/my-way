require 'spec_helper'
require 'game/levels'

module Game
  describe Levels do
    before :each do
      @window = MockWindow.new
    end

    context :title do
      it do
        levels = Levels.new @window
        levels.title.should be_a Title
      end
    end

    context :size do
      it do
        levels = Levels.new @window
        levels.size.should == 3
      end
    end

    context :convert do # replace
      it do
        data   = File.join ROOT, 'spec/levels/01.dat'
        levels = Levels.new @window, data, 'data'
        level  = levels.convert.first.map { |o| [o.x, o.y, o.f] }
        level.should == [
          Obstacle.new(@window, 0, 0, Image.new(@window, IMAGES_LIST[0]), 0),
          Obstacle.new(@window, 1, 0, Image.new(@window, IMAGES_LIST[1]), 0),
        ].map { |o| [o.x, o.y, o.f] }
      end
    end

    context :import do
      it do
        levels = Levels.new @window, 'data', 'data'
        levels.size.should == 2
      end

      it do
        data   = File.join ROOT, 'spec/levels/plain.dat'
        levels = Levels.new @window, data, 'data'
        levels.import
        levels.size.should == 1
      end

      it do
        data   = File.join ROOT, 'spec/levels/plain.dat'
        levels = Levels.new @window, data
        levels.import.should == []
      end

      it do
        data   = File.join ROOT, 'spec/levels/n.dat'
        levels = Levels.new @window, data
        levels.import.should == [[nil, nil]]
      end

      it do
        data   = File.join ROOT, 'spec/levels/num.dat'
        levels = Levels.new @window, data
        levels.import.should == [ [0, nil], [nil, 1], [2, nil], [nil, 3], [4, nil], [nil, 5], [6, nil], [nil, 7], [8, nil], [nil, 9] ]
      end

      it do
        data   = File.join ROOT, 'spec/levels/abc.dat'
        levels = Levels.new @window, data
        levels.import.should == [[10,11,12,13,14,15]]
      end
    end
  end
end
