require 'yaml'
require 'game/constructs'

class Title
  def initialize window
  end
end

class Score
  def initialize window
  end
end

class Image
  attr_reader :file_name
  attr_accessor :width, :height
  def initialize window, file_name = 'fake_path', tileable = false
    @file_name = file_name
    @width, @height = 0, 0
  end
end

class Player
  attr_writer :crash, :color
  attr_reader :window
  def initialize
    @window = MockWindow.new
  end

  def speed
    1
  end

  def color
    :white
  end

  def damage! color
  end

  def crashed? obstacle
    !!@crash
  end
end

module Game
  class Obstacle
    attr_reader :x, :y
    def initialize window, x, y, image
      @x, @y, @image = x, y, image
    end

    def f
      @image.file_name
    end
  end
end

class Level
end

class MockWindow
  attr_accessor :score

  def initialize
    @score = 0
  end
end
