require "game"

class LevelTest < Test::Unit::TestCase
  def setup
    @window = Game::MainWindow.new
    @data = [
      [0, 1],
      [2, 0],
    ]
    @level = Game::Level.new(@window, @data)
  end

  def test_window
    assert_equal(@window, @level.window)
  end

  def test_list
    assert_equal(2, @level.list.size)
  end

  def test_active
    assert_equal([], @level.active)
  end

  def test_myship
    assert_not_nil(@level.myship)
  end

  def test_update
    # FIXME
    assert_nothing_raised do
      @level.update
    end
  end

  def test_draw
    # FIXME
    assert_nothing_raised do
      @level.draw
    end
  end

  def test_cleared
    assert_false(@level.cleared?)
  end

  def test_game_over?
    assert_false(@level.game_over?)
  end

  def test_switch_player
    # FIXME
    assert_nothing_raised do
      @level.switch_player
    end
  end

  def test_button_up
    # TODO: validate called switch_player
    assert_nothing_raised do
      @level.button_up(Gosu::KbS)
      @level.button_up(Gosu::GpButton1)
    end
  end
end
