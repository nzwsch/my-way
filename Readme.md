# MY WAY

This program was made for [Open Source Conference 2014 Hokkaido](https://www.ospn.jp/osc2014-do/) to demonstrate submitting high score to [Sinatra](http://www.sinatrarb.com/).

## How to play

**pre-requirements**

We are using [Redis](http://redis.io/) and [Ohm](http://ohm.keyvalue.org/) as database. You can also check out [Sinatra Recipes](http://recipes.sinatrarb.com/p/models/ohm?#article).

```
# Install gems
$ gem install sinatra gosu ohm

# open Sinatra
$ ruby server.rb

# Play it
$ ./bin/game
```

When you reached to 2nd level, you can registered your score.

## Frequently Asked Questions [you can ask sending pull request]

Q. Is this still development?
A. **Nope**. This is just a demo. (But we had a lot of fun!)

Q. Can we make original level?
A. We create levels with [GridPatternEditor](https://github.com/myokoym/grid_pattern_editor) by [@myokoym](https://github.com/myokoym). You can look up and replace at levels directory.

Q. What the heck are there spec and test directory.
A. We don't have a plan how to test them.

Q. My display name was strange.
A. I've forgotten to put "U". That was st*v*pid.

Q. And they don't work.
A. I gave up.

Q. I don't know how to control them.
A. That's the why every modern video games have tutorial part.

Q. Window is a really big (I can't even see my character or fps ploblem).
A. This game was designed my PC. Sorry about that.

Q. Graphics are (*really*) sucks.
A. I know. You can improve them by sending pull request.

## Credit

### Development and Special thanks

* [Masafumi Yokoyama](https://github.com/myokoym)
* [HAYASAKA Ryosuke](https://github.com/HAYASAKA-Ryosuke)

### Library and Fonts

* [Sinatra](http://www.sinatrarb.com/)
* [Gosu](http://www.libgosu.org/)
* [Ohm](ohm.keyvalue.org)
* [Miffies](http://mfs.jp.org/)
* [Clutchee](http://www.fontsquirrel.com/fonts/Clutchee)

### My way

* [Frank Sinatra](http://www.youtube.com/watch?v=ePs6bHsQx6A)
