require 'digest/sha1'
require 'json'
require 'ohm'
require 'sinatra'

CHAR_INDEX = [*'A'..'Z'] + ['.', ' ']

Ohm.redis = Redic.new(ENV['REDISTOGO_URL'] || "redis://127.0.0.1:6379")

class User < Ohm::Model
  attribute :name
  attribute :data
  attribute :score

  index :name
end

before do
  @users = User.all.sort_by :score, order: 'DESC'
end

get '/' do
  haml :index
end

get '/scores.json' do
  content_type :json
  @users.map do |user|
    {
      name:  user.data.split('.').map { |i| i.to_i },
      score: user.score.to_i
    }
  end.first(10).to_json
end

post '/register' do
  # halt 401 unless auth_token? params[:token]

  User.create(
    name:  parse(params[:name]),
    data:  params[:name],
    score: params[:score]
  )
end

post '/flush' do
  # halt 401 unless auth_token? params[:token] if ENV['REDISTOGO_URL']

  Ohm.flush
end

helpers do
  def auth_token? string
    string == Digest::SHA1.hexdigest(ENV['TOKEN'])
  end

  def parse string
    string.split('.').map { |i| CHAR_INDEX[i.to_i] }.join
  end
end
