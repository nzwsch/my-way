begin
  require "gosu"
rescue LoadError
  $stderr.puts(<<-END_OF_MESSAGE)
  LoadError: #{$!.message}. Please try `gem install gosu`.
    If you are using Linux, please see https://github.com/jlnr/gosu/wiki/Getting-Started-on-Linux
  END_OF_MESSAGE
  exit(false)
end

module Firehats
  module ZOrder
    Background, Object, Text = *0..2
  end

  module Entity
    attr_reader :x, :y

    def initialize(window)
      @window = window
      @x = 0
      @y = 0
    end

    def warp(x, y)
      @x = x
      @y = y
    end
  end

  module Jewel
    include Entity

    private
    def draw_body
      draw_upper_body
      draw_lower_body
    end

    def draw_upper_body
      x1 = @x - @radius * 0.5
      y1 = @y - @radius * 0.7
      x2 = @x + @radius * 0.5
      y2 = @y - @radius * 0.7
      x3 = @x + @radius
      y3 = @y - @radius * 0.1
      x4 = @x - @radius
      y4 = @y - @radius * 0.1
      @window.draw_quad(x1, y1, @color,
                        x2, y2, @color,
                        x3, y3, @color,
                        x4, y4, @color,
                        ZOrder::Object)
    end

    def draw_lower_body
      x1 = @x - @radius
      y1 = @y
      x2 = @x + @radius
      y2 = @y
      x3 = @x
      y3 = @y + @radius
      @window.draw_triangle(x1, y1, @color,
                            x2, y2, @color,
                            x3, y3, @color,
                            ZOrder::Object)
    end
  end

  class Player
    include Jewel

    attr_reader :angle

    def initialize(window)
      super(window)
      @radius = @window.width * 0.03
      @movement = @window.width * 0.005
      @color = Gosu::Color::RED
      @angle = 0
    end

    def update
      if move_to_left?
        @x -= @movement
        @angle -= 1
        @angle = -30 if @angle < -30
      elsif move_to_right?
        @x += @movement
        @angle += 1
        @angle = 30 if @angle > 30
      else
        @angle = 0
      end
    end

    def draw
      @window.rotate(@angle, @x, @y) do
        draw_body
      end
    end

    private
    def move_to_left?
      @window.button_down?(Gosu::KbLeft) or
      @window.button_down?(Gosu::MsLeft) or
      @window.button_down?(Gosu::GpLeft)
    end

    def move_to_right?
      @window.button_down?(Gosu::KbRight) or
      @window.button_down?(Gosu::MsRight) or
      @window.button_down?(Gosu::GpRight)
    end
  end

  class Bullet
    include Jewel

    def initialize(window, angle=0)
      super(window)
      @radius = @window.width * 0.01
      @movement = @window.width * 0.015
      @color = Gosu::Color::RED
      @angle = angle
    end

    def move
      @x += Gosu.offset_x(@angle, @movement)
      @y += Gosu.offset_y(@angle, @movement)
    end

    def update
      move
      @movement *= 0.99
    end

    def draw
      draw_body
    end
  end

  class Hat
    include Entity

    attr_reader :scale

    def initialize(window, scale=1.0)
      super(window)
      @scale = scale
      @radius = @window.width * 0.03 * @scale
      @movement = @window.width * 0.005
      @color = Gosu::Color::GRAY
      @burn = false
    end

    def burn?
      @burn
    end

    def burn!
      @color = Gosu::Color::RED
      @burn = true
    end

    def stop!
      @movement = 0
    end

    def update
      @y += @movement
    end

    def draw
      draw_crown
      draw_ribbon
      draw_edge
    end

    private
    def draw_crown
      x1 = @x - @radius * 0.8
      y1 = @y - @radius * 0.7
      x2 = @x + @radius * 0.8
      y2 = @y + @radius * 0.5
      draw_rectangle(x1, y1, x2, y2, @color)
    end

    def draw_ribbon
      x1 = @x - @radius * 0.75
      y1 = @y - @radius * 0.1
      x2 = @x + @radius * 0.75
      y2 = @y + @radius * 0.4
      draw_rectangle(x1, y1, x2, y2, Gosu::Color::BLACK)
    end

    def draw_edge
      x1 = @x - @radius * 1.1
      y1 = @y + @radius * 0.4
      x2 = @x + @radius * 1.1
      y2 = @y + @radius * 0.7
      draw_rectangle(x1, y1, x2, y2, @color)
    end

    def draw_rectangle(x1, y1, x2, y2, color)
      @window.draw_quad(x1, y1, color,
                        x2, y1, color,
                        x2, y2, color,
                        x1, y2, color,
                        ZOrder::Object)
    end
  end

  class Window < Gosu::Window
    def initialize(width=640, height=480)
      super(width, height, false)
      self.caption = "Fire Hats"
      @radius = width * 0.03
      @night_color = Gosu::Color::BLACK
      @daytime_color = Gosu::Color.new(0xff0066ff)
      @font = Gosu::Font.new(self,
                             Gosu.default_font_name,
                             width / 20)
      create_hat_positions
      init
    end

    def pause
      @pause = !@pause
    end

    def update
      return if @pause
      update_timer
      @player.update
      shoot if rand(2) == 1 if button_down?(Gosu::KbF)
      update_hats
      update_bullets
      update_camera
    end

    def draw
      translate(-@camera_x, -@camera_y) do
        draw_background
        @player.draw
        @hats.each {|hat| hat.draw }
        @bullets.each {|bullet| bullet.draw }
      end
      draw_score
      draw_time
      draw_message if @game_over
    end

    def button_down(id)
      case id
      when Gosu::KbZ, Gosu::MsWheelUp, Gosu::GpButton1
        shoot
      when Gosu::KbSpace
        pause
      when Gosu::KbR
        init
      when Gosu::KbS
        get_screenshot
      when Gosu::KbQ, Gosu::KbEscape
        close
      end
    end

    private
    def init
      @player = Player.new(self)
      @player.warp(width * 0.5, height * 0.8)
      @hats = []
      @bullets = []
      @score = 0
      @time = 60 * 60
      @game_over = false
      @camera_x = 0
      @camera_y = 0
      add_boss
    end

    def create_hat_positions
      @hat_positions = []
      1.upto(198) do |n|
        n.times do
          @hat_positions << n
        end
      end
    end

    def update_hats
      add_hat if rand(5) == 0

      @hats.each do |hat|
        hat.update
        if hat.burn?
          @hats.delete(hat)
        elsif Gosu.distance(@player.x, @player.y, hat.x, hat.y) < @radius
          hat.burn!
          @score -= 5
          @score = 0 if @score < 0
        elsif hat.y > height
          @hats.delete(hat)
        end
        @bullets.each do |bullet|
          if Gosu.distance(bullet.x, bullet.y, hat.x, hat.y) < @radius
            hat.burn!
            @score += (hat.scale ** 2).to_i
            @bullets.delete(bullet)
            break
          end
        end
      end
    end

    def update_bullets
      @bullets.each do |bullet|
        bullet.update
        if (bullet.x < 0 or bullet.x > width) and
          (bullet.y < 0 or bullet.y > height)
          @bullets.delete(bullet)
        end
      end
    end

    def add_hat
      hat = Hat.new(self)
      hat.warp(self.width * 0.01 * @hat_positions.sample, 0)
      @hats << hat
    end

    def add_boss
      boss = Hat.new(self, 9.0)
      boss.warp(self.width * 6, self.height * 0.3)
      boss.stop!
      @hats << boss
    end

    def shoot
      bullet = Bullet.new(self, @player.angle)
      bullet.warp(@player.x, @player.y)
      2.times { bullet.move }
      @bullets << bullet
    end

    def update_timer
      @time -= 1
      if @time <= 0
        game_over
      end
    end

    def update_camera
      margin = @radius * 2
      if (@player.x + margin) >= (@camera_x + width)
        @camera_x = @player.x + margin - width
      elsif (@player.x - margin) <= @camera_x
        @camera_x = @player.x - margin
      end
    end

    def draw_background
      x1 = width * -2
      y1 = 0
      x2 = width * 8
      y2 = height
      draw_quad(x1, y1, @night_color,
                x2, y1, @daytime_color,
                x2, y2, @daytime_color,
                x1, y2, @night_color,
                ZOrder::Background)
    end

    def draw_score
      @font.draw("Score: #{@score}",
                 10, 10,
                 ZOrder::Text,
                 1.0, 1.0,
                 Gosu::Color::YELLOW)
    end

    def draw_time
      sec = @time / 60
      msec = (@time % 60) * (1000 / 60)
      @font.draw("Time: #{sprintf("%02d.%03d", sec, msec)}",
                 10, 10 + width / 20,
                 ZOrder::Text,
                 1.0, 1.0,
                 Gosu::Color::GREEN)
    end

    def draw_message
      @font.draw("Game Over",
                 width * 0.2 , height * 0.3,
                 ZOrder::Text,
                 3.0, 3.0,
                 Gosu::Color::WHITE)

      @font.draw("Thank you for playing!",
                 width * 0.3 , height * 0.5,
                 ZOrder::Text,
                 1.0, 1.0,
                 Gosu::Color::WHITE)
    end

    def get_screenshot
      begin
        require "gl"
        require "gdk_pixbuf2"
      rescue LoadError
        $stderr.puts("Warning: couldn't take a screenshot (#{$!.message}). Please try `gem install opengl gdk_pixbuf2`.")
        return
      end

      data = GL.ReadPixels(0, 0,
                           width - 1, height - 1,
                           GL::GL_RGB,
                           GL::GL_UNSIGNED_BYTE)

      screen_buffer = Gdk::Pixbuf.new(data,
                                      Gdk::Pixbuf::COLORSPACE_RGB,
                                      false,
                                      8,
                                      width - 1, height - 1,
                                      (width - 1) * 3)

      screen_buffer.flip(false).save("screenshot.png", "png")
    end

    def game_over
      pause
      @game_over = true
      register_score
    end

    def register_score
      # FIXME
      Thread.new do
        require "open-uri"
        open("http://localhost:9393/#{@score}")
      end
    end
  end
end

Firehats::Window.new(640, 640).show
