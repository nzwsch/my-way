require "gosu"

module InputName
  module ZOrder
    Background, Text, Cursor = *0..2
  end

  class Window < Gosu::Window
    def initialize(width=240, height=160)
      super(width, height, false)
      self.caption = "Input Name Sample"
      @alphabets = *"A".."Z"
      @font = Gosu::Font.new(self, "IPAGothic", height)
      @font_factor_x = width.to_f / (height * 1.5)
      @indexes = Array.new(3) { 0 }
      @cell_width = width / @indexes.size
      @cursor = 0
      @cursor_color = Gosu::Color.new(0x33ffffff)
    end

    def name
      @indexes.collect {|index| @alphabets[index] }.join
    end

    def update
    end

    def draw
      draw_alphabets
      draw_cursor
    end

    def button_down(id)
      case id
      when Gosu::KbUp
        change_alphabet(-1)
      when Gosu::KbDown
        change_alphabet(1)
      when Gosu::KbLeft
        move_corsor(-1)
      when Gosu::KbRight
        move_corsor(1)
      when Gosu::KbEnter, Gosu::KbReturn
        puts name
      when Gosu::KbQ, Gosu::KbEscape
        close
      end
    end

    private
    def change_alphabet(movement)
      @indexes[@cursor] += movement
      @indexes[@cursor] %= @alphabets.size
    end

    def move_corsor(movement)
      @cursor += movement
      @cursor %= @indexes.size
    end

    def draw_alphabets
      @indexes.each_with_index do |index, i|
        @font.draw(@alphabets[index],
                   @cell_width * i, height * 0.1,
                   ZOrder::Text,
                   @font_factor_x, 0.8,
                   Gosu::Color::WHITE)
      end
    end

    def draw_cursor
      x1 = @cell_width * @cursor
      x2 = @cell_width * (@cursor + 1)
      draw_square(x1, 0, x2, height, @cursor_color)

      x = (x1 + x2) / 2
      draw_cursor_arrows(x)
    end

    def draw_square(x1, y1, x2, y2, color)
      draw_quad(x1, y1, color,
                x2, y1, color,
                x2, y2, color,
                x1, y2, color,
                ZOrder::Cursor)
    end

    def draw_cursor_arrows(x)
      x1 = x
      y1 = height * 0.05
      x2 = x - width * 0.05
      y2 = height * 0.1
      x3 = x + width * 0.05
      y3 = height * 0.1
      draw_triangle(x1, y1, Gosu::Color::GRAY,
                    x2, y2, Gosu::Color::GRAY,
                    x3, y3, Gosu::Color::GRAY,
                    ZOrder::Cursor)

      y1 = height - y1
      y2 = height - y2
      y3 = height - y3
      draw_triangle(x1, y1, Gosu::Color::GRAY,
                    x2, y2, Gosu::Color::GRAY,
                    x3, y3, Gosu::Color::GRAY,
                    ZOrder::Cursor)
    end
  end
end

InputName::Window.new.show
