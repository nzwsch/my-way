require "gosu"
require "open-uri"
require 'net/http'

module CatchHats
  module ZOrder
    Background, Object, Text = *0..2
  end

  class Player
    attr_reader :x, :y

    def initialize(window)
      @window = window
      @x = 0
      @y = 0
      @radius = @window.width * 0.03
      @movement = @window.width * 0.005
      @color = Gosu::Color::RED
    end

    def warp(x, y)
      @x = x
      @y = y
    end

    def update
      @x -= @movement if move_to_left?
      @x += @movement if move_to_right?
    end

    def draw
      draw_upper_body
      draw_lower_body
    end

    private
    def move_to_left?
      @window.button_down?(Gosu::KbLeft)  ||
       @window.button_down?(Gosu::MsLeft) ||
       @window.button_down?(Gosu::GpLeft)
    end

    def move_to_right?
      @window.button_down?(Gosu::KbRight)  ||
       @window.button_down?(Gosu::MsRight) ||
       @window.button_down?(Gosu::GpRight)
    end

    def draw_upper_body
      x1 = @x - @radius * 0.5
      y1 = @y - @radius * 0.7
      x2 = @x + @radius * 0.5
      y2 = @y - @radius * 0.7
      x3 = @x + @radius
      y3 = @y - @radius * 0.1
      x4 = @x - @radius
      y4 = @y - @radius * 0.1
      @window.draw_quad(x1, y1, @color,
                        x2, y2, @color,
                        x3, y3, @color,
                        x4, y4, @color,
                        ZOrder::Object)
    end

    def draw_lower_body
      x1 = @x - @radius
      y1 = @y
      x2 = @x + @radius
      y2 = @y
      x3 = @x
      y3 = @y + @radius
      @window.draw_triangle(x1, y1, @color,
                            x2, y2, @color,
                            x3, y3, @color,
                            ZOrder::Object)
    end
  end

  class Hat
    attr_reader :x, :y

    def initialize(window)
      @window = window
      @x = 0
      @y = 0
      @radius = @window.width * 0.03
      @movement = @window.width * 0.005
      @color = Gosu::Color::GRAY
    end

    def warp(x, y)
      @x = x
      @y = y
    end

    def update
      @y += @movement
    end

    def draw
      draw_crown
      draw_ribbon
      draw_edge
    end

    private
    def draw_crown
      x1 = @x - @radius * 0.8
      y1 = @y - @radius * 0.7
      x2 = @x + @radius * 0.8
      y2 = @y + @radius * 0.5
      draw_square(x1, y1, x2, y2, Gosu::Color::GRAY)
    end

    def draw_ribbon
      x1 = @x - @radius * 0.75
      y1 = @y - @radius * 0.1
      x2 = @x + @radius * 0.75
      y2 = @y + @radius * 0.4
      draw_square(x1, y1, x2, y2, Gosu::Color::BLACK)
    end

    def draw_edge
      x1 = @x - @radius * 1.1
      y1 = @y + @radius * 0.4
      x2 = @x + @radius * 1.1
      y2 = @y + @radius * 0.7
      draw_square(x1, y1, x2, y2, Gosu::Color::GRAY)
    end

    def draw_square(x1, y1, x2, y2, color)
      @window.draw_quad(x1, y1, color,
                        x2, y1, color,
                        x2, y2, color,
                        x1, y2, color,
                        ZOrder::Object)
    end
  end

  class Window < Gosu::Window
    def initialize(width=640, height=480)
      super(width, height, false)
      self.caption = "Catch Hats"
      @radius = width * 0.03
      @player = Player.new(self)
      @font = Gosu::Font.new(self,
                             Gosu.default_font_name,
                             width / 20)
      init
    end

    def pause
      @pause = !@pause
    end

    def update
      return if @pause
      update_timer
      @player.update
      update_hats
    end

    def draw
      @player.draw
      @hats.each {|hat| hat.draw }
      draw_score
      draw_time
      draw_message if @game_over
    end

    def button_down(id)
      case id
      when Gosu::KbSpace
        pause
      when Gosu::KbR
        init
      when Gosu::KbQ, Gosu::KbEscape
        close
      end
    end

    private
    def init
      @player.warp(width * 0.5, height * 0.8)
      @hats = []
      @score = 0
      @time = 30 * 60
      @game_over = false
    end

    def update_hats
      add_hat if rand(10) == 0

      @hats.each do |hat|
        hat.update
        if Gosu.distance(@player.x, @player.y, hat.x, hat.y) < @radius
          @hats.delete(hat)
          @score += 1
        elsif hat.y > height
          @hats.delete(hat)
        end
      end
    end

    def add_hat
      hat = Hat.new(self)
      hat.warp(self.width * 0.01 * rand(100), 0)
      @hats << hat
    end

    def update_timer
      @time -= 1
      if @time <= 0
        game_over
      end
    end

    def draw_score
      @font.draw("Score: #{@score}",
                 10, 10,
                 ZOrder::Text,
                 1.0, 1.0,
                 Gosu::Color::YELLOW)
    end

    def draw_time
      sec = @time / 60
      msec = (@time % 60) * (1000 / 60)
      @font.draw("Time: #{sprintf("%02d.%03d", sec, msec)}",
                 10, 10 + width / 20,
                 ZOrder::Text,
                 1.0, 1.0,
                 Gosu::Color::GREEN)
    end

    def draw_message
      @font.draw("Game Over",
                 width * 0.2 , height * 0.3,
                 ZOrder::Text,
                 3.0, 3.0,
                 Gosu::Color::WHITE)
      @font.draw("Thank you for playing!",
                 width * 0.3 , height * 0.5,
                 ZOrder::Text,
                 1.0, 1.0,
                 Gosu::Color::WHITE)
    end

    def game_over
      pause
      @game_over = true
      register_score
    end

    def register_score
      uri = URI.prase "http://localhost:9393/" # thanks for using Shotgun!
      Net::HTTP.post_form uri, score: @score.to_s
    end
  end
end

CatchHats::Window.new.show
