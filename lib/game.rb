require 'gosu'
require 'yaml'

include Gosu

require_relative 'ext/float'
require_relative 'game/constructs'
require_relative 'score'
require_relative 'game/counter'
require_relative 'game/screen'
require_relative 'game/title'
require_relative 'game/timer'
require_relative 'game/timer_register'
require_relative 'game/meter'
require_relative 'game/stats'
require_relative 'game/player'
require_relative 'game/effect'
require_relative 'game/obstacle'
require_relative 'game/obstacle_item'
require_relative 'game/obstacle_time'
require_relative 'game/register'
require_relative 'game/background'
require_relative 'game/level'
require_relative 'game/levels'
require_relative 'game/window'
require_relative 'game/perspective'
require_relative 'game/debugger'

module Game
  def self.new debug = false
    debug ? DebugWindow.new : MainWindow.new
  end
end
