module Game
  class Player
    attr_reader :window, :pos_x, :pos_y, :speed, :effects, :color

    def initialize window
      @window  = window
      @pos_x   = 336  # player x
      @pos_y   = 912  # player y
      @log     = 1.0  # using Math.log
      @meter   = 10.0 # base
      @speed   = 10
      @dist    = 0
      @index   = 0
      @status  = :idol
      @images  = Image.load_tiles window, PLAYER_IMAGE, 64, 105, false
      @effects = Array.new
      @counter = Counter.new
      @color   = toggle_color
    end

    def update
      @counter.update
      @effects.each(&:update).keep_if &:alive?

      up   if up?
      down if down?
      @meter -= 0.1 if !up? && !down? && @meter > 10.1

      left  if left?
      right if right?
      @status = :idol if !left? && !right?

      set_speed
    end

    def draw
      b = up? ? 100 : 200
      n = milliseconds / b % 4
      @effects.each &:draw
      @images[get_range][n].draw @pos_x, @pos_y, 2
    end

    def crashed? o
      p = col_state
      p[0] < o[1] && o[0] < p[1] && p[2] < o[3] && o[2] < p[3]
    end

    def damage! damage
      @window.blood =  256
      @window.score -= damage
    end

    def switch
      return unless switchable?
      @window.score -= PLAYER_SWITCH_COST
      @counter = Counter.new
      @color   = toggle_color
    end

    def activate?
      dist =  @dist / ACTIVATE_INTERVAL
      return false if @index == dist
      @index = dist
      true
    end

    def switchable?
      return false if @window.level.stage_cleared? || @window.level.game_over?
      @window.score > 50 && @counter.second > 0
    end

    private

    def col_state
      [@pos_x + 16, @pos_x + 48, @pos_y + 16, @pos_y + 48]
    end

    def toggle_color
      @color == :white ? :black : :white
    end

    def get_range
      n = 0
      n = 4   if @status == :right
      n = 8   if @status == :left
      n += 12 if @color  == :white
      n..(n + 3)
    end

    def set_speed
      @log   = Math.log10 @meter
      @speed = (10 * @log).to_i
      @dist += @speed
      @window.score += @speed / 60.0
    end

    def up
      @meter += 0.5 if @meter < 100
    end

    def down
      @meter -= 1 if @meter > 11
    end

    def left
      @status, x = :left, (X_MAX_AXL - @log).to_i
      @pos_x -= x if @pos_x > x
    end

    def right
      @status, x = :right, (X_MAX_AXL - @log).to_i
      @pos_x += x if @pos_x < 704 - x
    end

    def up?
      @window.button_down?(KbUp) || @window.button_down?(GpUp)
    end

    def down?
      @window.button_down?(KbDown) || @window.button_down?(GpDown)
    end

    def left?
      @window.button_down?(KbLeft) || @window.button_down?(GpLeft)
    end

    def right?
      @window.button_down?(KbRight) || @window.button_down?(GpRight)
    end
  end
end
