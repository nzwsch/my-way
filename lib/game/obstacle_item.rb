module Game
  class ObstacleItem < Obstacle
    def update myship
      move myship.speed
      return if !@active || !myship.crashed?(square)
      return unless @color == myship.color or @color.nil?

      myship.window.score += @value
      myship.effects.push Effect.new(myship)
      hide
    end

    def draw
      @image.draw @pos_x, @pos_y, 1 if @active
    end

    private

    def square
      [@pos_x - 8, @pos_x + 56, @pos_y, @pos_y]
    end
  end
end
