require "matrix"

module Game
  module Perspective

    module_function

    def normal_projection
      [
        1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 1.0,
      ]
    end

    def entity_projection window, y, gap_y=0.0
      width  = STATS_X
      height = window.height

      if y > 0.0
        scale = y.to_f / height
      else
        scale = 0.0
      end
      trans_x = (width * 0.5) * (1.0 - scale)
      trans_y = (height * gap_y) * (1.0 - scale)
      [
        scale,   0.0,     0.0, 0.0,
        0.0,     scale,   0.0, 0.0,
        0.0,     0.0,     0.0, 0.0,
        trans_x, trans_y, 0.0, 1.0,
      ]
    end

    def background_projection window, gap_x=0.3, gap_y=0.0
      width  = STATS_X
      height = window.height

      src = [
        width * 0.0, height * 0.0,
        width * 1.0, height * 0.0,
        width * 1.0, height * 1.0,
        width * 0.0, height * 1.0,
      ]

      dest = [
        width * gap_x,         height * gap_y,
        width * (1.0 - gap_x), height * gap_y,
        width * 1.0,           height * 1.0,
        width * 0.0,           height * 1.0,
      ]

      param = homography_parameter *src, *dest
      [
        param[0], param[3], 0.0, param[6],
        param[1], param[4], 0.0, param[7],
        0.0,      0.0,      0.0, 0.0,
        param[2], param[5], 0.0, 1.0,
      ]
    end

    # 変換前後の座標から射影変換のパラメーターを求める
    #   x11-x14: 変換前のX座標（左上から時計回り）
    #   y11-y14: 変換前のY座標
    #   x21-x24: 変換後のX座標
    #   y21-y24: 変換後のY座標
    def homography_parameter x11, y11, x12, y12,
                             x13, y13, x14, y14,
                             x21, y21, x22, y22,
                             x23, y23, x24, y24

      # 係数行列
      coefficient_matrix = Matrix[
        [x11, y11, 1.0, 0.0, 0.0, 0.0, -x11 * x21, -y11 * x21],
        [0.0, 0.0, 0.0, x11, y11, 1.0, -x11 * y21, -y11 * y21],
        [x12, y12, 1.0, 0.0, 0.0, 0.0, -x12 * x22, -y12 * x22],
        [0.0, 0.0, 0.0, x12, y12, 1.0, -x12 * y22, -y12 * y22],
        [x13, y13, 1.0, 0.0, 0.0, 0.0, -x13 * x23, -y13 * x23],
        [0.0, 0.0, 0.0, x13, y13, 1.0, -x13 * y23, -y13 * y23],
        [x14, y14, 1.0, 0.0, 0.0, 0.0, -x14 * x24, -y14 * x24],
        [0.0, 0.0, 0.0, x14, y14, 1.0, -x14 * y24, -y14 * y24],
      ]

      # 定数ベクトル
      constant_vector = Vector[
        x21,
        y21,
        x22,
        y22,
        x23,
        y23,
        x24,
        y24,
      ]

      # 逆行列を使って連立方程式を解く
      parameter = coefficient_matrix.inverse * constant_vector

      # 実数の配列に変換
      parameter.to_a.map &:to_f
    end
  end
end
