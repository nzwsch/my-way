module Game
  class ObstacleTime < Obstacle
    def update myship
      move myship.speed
      return if !@active || !myship.crashed?(square)

      myship.window.level.stats.timer.add @value
      hide
    end

    def draw
      @image.draw @pos_x, @pos_y, 1 if @active
    end
  end
end
