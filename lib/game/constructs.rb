module Game
  ROOT = File.expand_path '../../..', __FILE__

  # importer.rb
  LEVELS_DIR = ROOT + '/levels'
  IMAGES_DIR = ROOT + '/media/images'
  FONTS_DIR  = ROOT + '/media/fonts'
  LEVELS     = [
    LEVELS_DIR + '/level1.dat',
    LEVELS_DIR + '/level2.dat',
    LEVELS_DIR + '/level3.dat'
  ]

  # screen.rb
  IMAGES_INDEX      = [*'0'..'9'] + [*'A'..'F']
  IMAGE_STAGE_CLEAR = IMAGES_DIR + '/stage_clear.png'
  IMAGE_GAME_OVER   = IMAGES_DIR + '/game_over.png'
  IMAGE_LOAD_SCREEN = IMAGES_DIR + '/load_screen.png'
  IMAGE_READY_SET   = IMAGES_DIR + '/ready_set.png'

  # level.rb
  ACTIVATE_INTERVAL = 400
  WAIT_START_GAME   = 3
  WAIT_RESET_GAME   = 0
  BACKGROUND_IMAGE  = IMAGES_DIR + '/background.png'

  # title.rb
  TITLE_LOGO       = IMAGES_DIR + '/title.png'
  MENU_LOGO        = IMAGES_DIR + '/menu.png'
  MENU_Y_POSITION  = 600
  ORDER_TITLE      = 5

  # player.rb
  X_ITEM_AXL   = 10
  X_MAX_AXL    = 8
  LIMIT_METER  = 100
  PLAYER_IMAGE = IMAGES_DIR + '/myship.png'
  PLAYER_SPEED = 5
  PLAYER_SWITCH_COST = 50

  # stats.rb
  STATS_IMAGE    = IMAGES_DIR + '/stats.png'
  STATS_X        = 768
  STATS_X_MARGIN = 784

  # obstract.rb
  OBSTACLE_LIST = YAML.load_file ROOT + '/config/obstacle_list.yml'

  # register.rb
  BAD_WORD = YAML.load_file ROOT + '/config/list_of_bad_word.yml'

  # timer.rb
  TIMER_FONT_NAME = 'media/fonts/m12.ttf'
  TIMER_FONT_SIZE = 96
  DEFAULT_TIMER   = 99

  # window.rb
  DEFAULT_SCORE = 0.01
  FONT_NAME     = FONTS_DIR + '/m41.ttf'
  FONT_SIZE     = 30
  GAME_TITLE    = 'MY WAY'

  # debugger.rb
  DEBUG_FONT_SIZE = 30
end
