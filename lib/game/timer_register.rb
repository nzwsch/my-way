module Game
  class TimerRegister < Timer
    attr_reader :counter

    def initialize window
      @window  = window
      @font    = Font.new @window, TIMER_FONT_NAME, TIMER_FONT_SIZE
      @time    = 99
      @counter = Counter.new
      @x, @y   = calc_x, 96
    end

    def update
      @counter.update
      count_down if @counter.reset?
    end

    def draw
      @font.draw @time.to_s, @x, @y, 5, 1, 1, Color::WHITE
    end

    def add time
      @time += time
    end

    def decrease
      @time -= 1
    end

    def is_zero?
      @time == 0
    end

    private

    def count_down
      @time -= 1 if @time > 0
      @x = calc_x
    end

    def calc_x
      (STATS_X - @font.text_width(@time.to_s)) / 2
    end
  end
end
