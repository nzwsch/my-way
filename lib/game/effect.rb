module Game
  class Effect
    attr_reader :life

    def initialize myship, image = '/score_up.png'
      @images = Image.load_tiles myship.window, IMAGES_DIR + image, 64, 64, false
      @animation = 0
      @x, @y = myship.pos_x, myship.pos_y
      @life = 20
    end

    def update
      @life -= 1 if alive?
      @y -= 0.3
      @animation = milliseconds / 50 % @images.size
    end

    def draw
      @images[@animation].draw @x, @y, 5 if alive?
    end

    def alive?
      @life > 0
    end
  end
end
