module Game
  class Screen
    attr_reader :window, :counter

    def initialize window
      @window      = window
      @counter     = Counter.new
      @stage_clear = Image.new @window, IMAGE_STAGE_CLEAR, false
      @game_over   = Image.new @window, IMAGE_GAME_OVER,   false
      @ready_set   = Image.load_tiles @window, IMAGE_READY_SET, 324, 60, false
    end

    def update
      @counter.update
    end

    def draw
    end

    def finished?
      @finish
    end

    def title?
      @title
    end

    def reset?
      @reset
    end

    def button_down id
    end

    def button_up id
    end

    def finished!
      @finish = true
    end

    def title!
      @title = true
    end

    def reset!
      @reset = true
    end
  end
end
