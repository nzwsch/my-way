module Game
  class Counter
    attr_reader :count, :interval, :second

    def initialize interval = 59
      @count    = 0
      @second   = 0
      @interval = interval
    end

    def update
      @count = reset? ? reset : @count + 1
    end

    def reset?
      @count == @interval
    end

    private

    def reset
      @second += 1
      0
    end
  end
end
