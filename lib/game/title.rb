module Game
  class Title < Screen
    def initialize window
      super window

      @logo_my = Image.from_text window, "My  ", FONTS_DIR + '/clutchee.otf', 170
      @logo_way = Image.from_text window, "Way", FONTS_DIR + '/clutchee.otf', 170

      @logo_c     = 0
      @logo_my_x  = 0
      @logo_way_x = (@window.width - @logo_my.width)
      @logo_y     = 300

      @menu    = Image.new window, MENU_LOGO, false
      @menu_x  = (window.width  - @menu.width) / 2
      @menu_y  = MENU_Y_POSITION
      @menu_c  = 0
      @white   = 255
      
      @limit_left  = (@window.width-@logo_my.width-@logo_way.width)/2
      @limit_right = (@window.width-@logo_way.width+@logo_my.width)/2
    end

    def update
      super
      @logo_c += 6  if @logo_c < 255
      @logo_my_x  += 15 if @logo_my_x  < @limit_left
      @logo_way_x -= 15 if @logo_way_x > @limit_right
      menu = milliseconds / 800 % 2
      @menu_c = menu * 255
      @white -= 4 if @white > 0 and @logo_way_x < @limit_right
    end

    def draw
      @window.draw_quad(0, 0, Color::BLACK, @window.width, 0, Color::BLACK, 0, @window.height, Color::BLACK, @window.width, @window.height, Color::BLACK, 0)
      @logo_my.draw @logo_my_x, @logo_y, ORDER_TITLE, 1, 1, Color.rgba(255, 255, 255, @logo_c)
      @logo_way.draw @logo_way_x, @logo_y, ORDER_TITLE, 1, 1, Color.rgba(255, 255, 255, @logo_c)
      if @white > 0 and @logo_way_x <  (@window.width-@logo_way.width+@logo_my.width)/2
        draw_white
      end
      return if @counter.second < 1
      @menu.draw @menu_x, @menu_y, ORDER_TITLE, 1, 1, Color.rgba(255, 255, 255, @menu_c)
    end

    def draw_white
      white= Color.new @white, 255, 255, 255
      @window.draw_quad 0, 0, white, 1280, 0, white, 0, 1024, white, 1280, 1024, white, 6
    end
    def button_up id
      finished! if id && @counter.second > 0
    end
  end
end
