module Game
  class Obstacle
    attr_reader :pos_x, :pos_y

    def initialize window, x, y, image, color, value
      @x, @y  = x, y
      @pos_x  = x * image.width
      @pos_y  = 240
      @image  = image
      @active = true
      @color  = color
      @value  = value
    end

    def update myship
      move myship.speed
      return if !@active || !myship.crashed?(square)
      myship.damage! @value if @color != myship.color
      hide
    end

    def draw
      @image.draw @pos_x, @pos_y, 1
      draw_square if @window
    end

    def gone?
      @pos_y > 1024
    end

    private

    def move speed
      @pos_y += speed
    end

    def hide
      @active = false
    end

    def square
      [@pos_x, @pos_x + 48, @pos_y, @pos_y]
    end
  end
end
