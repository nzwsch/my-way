module Game
  class DebugWindow < MainWindow
    def initialize
      super
      self.caption = '[DEBUG] ' + caption
      @debugger = Font.new self, default_font_name, DEBUG_FONT_SIZE
    end

    def update
      return nil if @pause
      super
      @myship = @level.myship if @level.is_a? Level
    end

    def draw
      super
      debug_messages.each_with_index do |i, n|
        @debugger.draw i, 16, (n * DEBUG_FONT_SIZE), 10, 1.0, 1.0, Color::RED
      end
      debug_player if @level.is_a? Level
    end

    def button_up id
      super
      @pause = !@pause if id == KbP
    end

    private

    def debug_messages
      "
      fps: #{fps}
      ".gsub(/^ +/, '').split("\n").delete_if &:empty?
    end

    def debug_player
      c = Color.rgba(0, 255, 255, 200)
      x1, y1 = @myship.pos_x + 16, @myship.pos_y + 16
      x2, y2 = @myship.pos_x + 48, @myship.pos_y + 48 # a, b = 32, 32
      draw_quad x1, y1, c, x2, y1, c, x1, y2, c, x2, y2, c, 10
    end
  end
end
