module Game
  class Levels
    attr_reader :title

    def initialize window, *levels
      @window = window
      @images = load_images
      @title  = Title.new window
      @levels = levels.empty? ? LEVELS.dup : levels
    end

    def size
      @levels.size
    end

    def next_level
      Level.new @window, convert
    end

    private

    def import_level
      data = File.read @levels.shift
      data.split.map do |line|
        line.split(//).map do |char|
          IMAGES_INDEX.index char if char =~ /[0-9A-F]/
        end
      end
    end

    def convert
      import_level.each_with_index.map do |row, y|
        row.each_with_index.map do |col, x|
          next if col.nil?
          property = OBSTACLE_LIST[col]
          obstacle = eval "#{property['class']}"
          obstacle.new @window, x, y, @images[col], property['color'], property['value']
        end
      end
    end

    def load_images
      OBSTACLE_LIST.map do |property|
        path = File.join IMAGES_DIR, property['path']
        Image.new @window, path, false
      end
    end
  end
end
