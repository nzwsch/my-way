require 'open-uri'
require 'net/http'

module Game
  class MainWindow < Window
    attr_reader :level, :rank, :previous_score
    attr_writer :blood
    attr_accessor :index

    def initialize
      super 1280, 1024, false
      self.caption = GAME_TITLE
      reset_levels
      prepare_projection
    end

    def update
      reset_levels   if @level.title?
      reset_levels   if @level.reset?    && @levels.size + 1 == LEVELS.size
      next_level     if @level.finished? && @levels.size > 0
      register_score if @level.reset? || @level.finished?
      @blood -= 48   if @blood > 0
      @level.update
    end

    def draw
      draw_blood
      @level.draw
    end

    def button_down id
      close if id == KbEscape
      @level.button_down id
    end

    def button_up id
      @level.button_up id
    end

    def background_projection(&block)
      transform(*@background_projection) do
        scale(2.6, 1.0, STATS_X / 2, 0) do
          yield
        end
      end
    end

    def entity_projection(x, y, &block)
      transform(*@entity_projection[y]) do
        yield
      end
    end

    def scores
      @score
    end

    def score
      @score[@index]
    end

    def score= val
      @score[@index] = val
    end

    def send_data name
      url = URI.parse 'http://localhost:4567/register'
      begin
        Net::HTTP.post_form(url, {
          :name  => name.join('.'),
          :score => @score.inject(:+),
          # :token => ENV['TOKEN']
        })
        puts 'Score sended!'
      rescue
      end
    end

    private

    def register_score
      @level = Register.new self
    end

    def next_level
      return register_score if @levels.size == 0
      @index += 1
      @score << DEFAULT_SCORE
      @previous_score = @score.inject(:+)
      @level = @levels.next_level if @levels.size > 0
    end

    def reset_levels
      @index  = -1
      @blood  = 0
      @score  = []
      @rank   = Score::Rank.new
      @levels = Levels.new self
      @level  = @levels.title
    end

    def prepare_projection
      gap_x, gap_y = 0.3, 0.3
      @background_projection = Perspective.background_projection self, gap_x, gap_y
      @entity_projection = {}
      0.upto(self.height).each do |y|
        @entity_projection[y] = Perspective.entity_projection self, y, gap_y
      end
    end

    def draw_blood
      red = Color.new @blood, 255, 0, 0
      draw_quad 0, 0, red, STATS_X, 0, red, 0, height, red, STATS_X, height, red, 6
    end
  end
end
