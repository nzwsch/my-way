module Game
  class Stats
    attr_reader :timer

    def initialize window, myship = false
      @window = window
      @image  = Image.new window, STATS_IMAGE, false
      timer   = myship ? Timer : TimerRegister
      @timer  = timer.new window
      @font   = Font.new  window, FONT_NAME, FONT_SIZE

      return unless myship
      @myship = myship
      @meter  = Meter.new window
    end

    def update
      @timer.update
    end

    def draw
      @timer.draw
      @image.draw STATS_X, 0, 4
      score = @window.previous_score + @window.score
      draw_pair 16, 'High Score', @window.rank.high_score(score).to_score
      @window.scores.each_with_index do |float, index|
        draw_pair index * 76 + 92, "stage #{index + 1}", float.to_score
      end
      return unless @myship
      draw_meter 358, 'Speed',  @myship.speed.to_i
      draw_pair  434, 'Rest',   @window.level.remains.to_s + 'm'
      draw_pair  510, 'Switch', "#{@myship.switchable? ? 'Available' : ''}"
    end

    private

    def draw_text y, text, color
      @font.draw text, STATS_X_MARGIN, y, 5, 1.0, 1.0, color
    end

    def draw_pair y, red, white
      draw_text y,      red,   Color::RED
      draw_text y + 38, white, Color::WHITE
    end

    def draw_meter y, red, white
      @font.draw  red,   STATS_X_MARGIN, y,      5, 1.0, 1.0, Color::RED
      @meter.draw white, STATS_X_MARGIN, y + 38, 5
    end
  end
end
