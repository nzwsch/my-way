module Game
  METER_HEIGHT = 27
  METER_WIDTH  = 15
  METER_MARGIN = 20

  class Meter
    def initialize window
      @window = window
      @font   = Font.new window, FONT_NAME, FONT_SIZE
    end

    def draw val, x, y, z
      val = 19 if val == 20 # totally lame
      (val - 9).times do |n|
        c = Color::GREEN
        c = Color::YELLOW if n > 4
        c = Color::RED    if n > 7
        quad (METER_MARGIN * n) + x, y, z, c
      end
    end

    private

    def quad x, y, z, c, h = METER_HEIGHT, w = METER_WIDTH
      @window.draw_quad(
        x, y, c,
        x + w, y, c,
        x, y + h, c,
        x + w, y + h, c, z
      )
    end
  end
end
