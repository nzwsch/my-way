module Game
  class Level < Screen
    attr_reader :myship, :stats

    def initialize window, level
      super window
      @activated  = 0
      @list       = level
      @active     = Array.new
      @background = Background.new window
      @myship     = Player.new     window
      @stats      = Stats.new      window, @myship
      @font       = Font.new       window, FONT_NAME, FONT_SIZE
    end

    def update
      super
      return game_over     if game_over?
      return stage_cleared if stage_cleared?
      return if @counter.second < WAIT_START_GAME
      @myship.update
      @stats.update
      activate_line if @myship.activate?
      @active.each      { |l| l.each { |obs| obs.update @myship } }
      @active.delete_if { |l| l.all? { |obs| obs.gone? } }
    end

    def draw
      @game_over.draw   171, 362, 5 if game_over?
      @stage_clear.draw 171, 362, 5 if stage_cleared?
      @ready_set[@counter.second].draw 222, 482, 5 if @counter.second < @ready_set.size
      @background.draw
      @stats.draw
      @myship.draw if !game_over?
      @active.each do |line|
        line.each do |obs|
          @window.entity_projection(obs.pos_x, obs.pos_y) do
            obs.draw
          end
        end
      end
    end

    def button_up id
      game_over!     if id == KbReturn || id == GpButton1
      stage_cleared! if id == KbReturn || id == GpButton1
      @myship.switch if id == KbS      || id == GpButton1
    end

    def remains
      @list.size
    end

    def game_over?
      return false if stage_cleared? || @window.is_a?(DebugWindow)
      @window.score <= 0 || @stats.timer.is_zero?
    end

    def stage_cleared?
      @list.empty? && @active.empty?
    end

    private

    def game_over
      return unless game_over?
      @game_over_counter ||= Counter.new
      @game_over_counter.update
      reset! if @game_over_counter.second > 4
    end

    def game_over!
      return unless @game_over_counter
      reset! if     @game_over_counter.second > WAIT_RESET_GAME
    end

    def stage_cleared
      return unless stage_cleared?
      @stage_cleared_counter ||= Counter.new 2
      @stage_cleared_counter.update
      return unless @stage_cleared_counter.second > 60 # wait 2 * 60 sec
      bonus  if     @stage_cleared_counter.reset?
    end

    def stage_cleared!
      return    unless @stage_cleared_counter
      finished! if     @stage_cleared_counter.second > 60 && @stats.timer.is_zero?
    end

    def bonus
      return if @stats.timer.is_zero?
      @stats.timer.decrease
      @window.score += 100
    end

    def activate_line
      line = @list.pop
      @active << line.compact if line
    end
  end
end
