module Game
  class Register < Screen
    def initialize window
      super window
      @background     = Image.new         window, IMAGES_DIR + '/score_register.png',    true
      @score_chars    = Image.load_tiles  window, IMAGES_DIR + '/score_chars.png',       100, 120, false
      @register_chars = Image.load_tiles  window, IMAGES_DIR + '/score_chars_white.png', 100, 120, false
      @name  = []
      @stats = Stats.new window
      @index = 0
    end

    def update
      super
      return register! if @register_counter
      register         if @stats.timer.is_zero?
      @index = 27      if full?
      @stats.update
    end

    def draw
      @background.draw 0, 0, 0
      @stats.draw
      @name.each_with_index do |char, index|
        @register_chars[char].draw index * 120 + 205, 280, 1
      end
      @score_chars[@index].draw @index % 7 * 100 + 34, @index / 7 * 120 + 475, 1
    end

    def button_up id
      register if @index == 27 && return?(id)
      delete   if delete? id
      return   if full?
      push     if return? id
      up       if up?     id
      down     if down?   id
      left     if left?   id
      right    if right?  id
    end

    private

    def register
      @name = [rand(25), rand(27), rand(27)] if bad_word?
      @register_counter = Counter.new
    end

    def register!
      @register_counter.update
      return unless @register_counter.second > 1
      @window.send_data @name
      title!
    end

    def full?
      @name.size == 3
    end

    def bad_word?
      BAD_WORD.include?(@name) || @name.empty? || @name[0] > 24 || @name.all? { |n| n > 24 }
    end

    def up
      @index = @index > 6 ? @index - 7 : @index + 21
    end

    def down
      @index = @index < 21 ? @index + 7 : @index - 21
    end

    def left
      @index = @index % 7 == 0 ? @index + 6 : @index - 1
    end

    def right
      @index = @index % 7 == 6 ? @index - 6 : @index + 1
    end

    def push
      @name.push @index
    end

    def delete
      @name.pop unless @name.empty?
    end

    def up? id
      id == KbUp || id == GpUp
    end

    def down? id
      id == KbDown || id == GpDown
    end

    def left? id
      id == KbLeft || id == GpLeft
    end

    def right? id
      id == KbRight || id == GpRight
    end

    def delete? id
      id == KbDelete || id == KbBackspace || id == GpButton0
    end

    def return? id
      id == KbReturn || id == GpButton1
    end
  end
end
