module Game
  class Background
    def initialize window
      @window = window
      @image  = Image.new window, BACKGROUND_IMAGE, true
    end

    def draw
      draw_sky
      draw_cloud(0xeefefefe, 100, 30)

      @window.background_projection do # 0, -60
        draw_background
      end
    end

    private

    def draw_background
      draw_ground
      draw_tree
      draw_cloud(0x99333333, 70, 30)  # shadow
    end

    def draw_sky
      color = 0xff84cefb
      @window.draw_quad(0,       0,   color,
                        STATS_X, 0,   color,
                        STATS_X, 320, color,
                        0,       320, color, 0)
    end

    def draw_cloud(color, width, height)
      slowness = 50_000
      x = (STATS_X + width) * (milliseconds.to_f % slowness / slowness)
      y = 50
      @window.draw_quad(x,         y,          color,
                        x - width, y,          color,
                        x - width, y + height, color,
                        x,         y + height, color, 0)

      slowness /= 2
      x = (STATS_X + width) * (milliseconds.to_f % slowness / slowness)
      y = 150
      @window.draw_quad(x,         y,          color,
                        x - width, y,          color,
                        x - width, y + height, color,
                        x,         y + height, color, 0)
    end

    def draw_ground
      grass_color    = 0xff00cd5f
      road_color     = 0xffffe08c
      grass_padding  = 0.3
      border_left_x  = STATS_X * grass_padding
      border_right_x = STATS_X * (1.0 - grass_padding)

      @window.draw_quad(0,             0,    grass_color,
                        border_left_x, 0,    grass_color,
                        border_left_x, 1024, grass_color,
                        0,             1024, grass_color, 0)

      @window.draw_quad(border_left_x,  0,    road_color,
                        border_right_x, 0,    road_color,
                        border_right_x, 1024, road_color,
                        border_left_x,  1024, road_color, 0)

      @window.draw_quad(border_right_x, 0,    grass_color,
                        STATS_X,        0,    grass_color,
                        STATS_X,        1024, grass_color,
                        border_right_x, 1024, grass_color, 0)
    end

    def draw_tree
      color = 0xffbc5647
      # TODO
    end
  end
end
