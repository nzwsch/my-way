module Score
  class Rank
    attr_reader :list

    def initialize
      @list = YAML.load_file Game::ROOT + '/config/high_score.yml'
    end

    def high_score val
      score = @list.first[:score]
      score > val ? score : val
    end
  end
end
