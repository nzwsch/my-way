class Float
  def to_score
    self.to_i.to_score
  end
end

class Fixnum
  def to_score
    int = self > 0 ? self : 0
    "%06d00" % int
  end
end
